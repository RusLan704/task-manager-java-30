package ru.bakhtiyarov.tm.endpoint.calc;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface CalcEndpoint {

    @WebMethod
    int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b
    );

}
