package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.api.service.converter.IConverterLocator;
import ru.bakhtiyarov.tm.service.DomainService;

public interface ServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IConverterLocator getConverterLocator();

}
