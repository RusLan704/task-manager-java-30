package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IManagerFactoryService {

    void initManagerFactory();

    @NotNull
    EntityManager getEntityManager();

}
