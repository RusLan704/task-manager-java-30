package ru.bakhtiyarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void createProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    void createProjectByNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

    @NotNull
    @WebMethod
    List<ProjectDTO> findAllProjectsBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    );

    @WebMethod
    void removeAllProjectsBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    @Nullable
    @WebMethod
    ProjectDTO removeProjectOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    );

    @Nullable
    @WebMethod
    ProjectDTO removeProjectOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    ProjectDTO removeProjectOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    @Nullable
    @WebMethod
    ProjectDTO updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

    @Nullable
    @WebMethod
    ProjectDTO updateProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

}
