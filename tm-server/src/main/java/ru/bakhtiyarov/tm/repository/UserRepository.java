package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.IUserRepository;
import ru.bakhtiyarov.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final EntityManager em;

    @NotNull
    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
        em = entityManager;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        List<User> users = em.createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList();
        if (users.isEmpty()) return null;
        return users.get(0);
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        List<User> users = em.createQuery("SELECT e FROM User e WHERE e.id = :id", User.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (users.isEmpty()) return null;
        return users.get(0);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        final @Nullable User user = findByLogin(login);
        if (user == null) return null;
        em.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeById(@NotNull final String id) {
        final @Nullable User user = findById(id);
        if (user == null) return null;
        remove(user);
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return em.createQuery("SELECT e FROM User e", User.class).getResultList();
    }

    @NotNull
    @Override
    public List<User> removeAll() {
        final @Nullable List<User> users = findAll();
        users.forEach(this::remove);
        return users;
    }

}
