package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.api.repository.ISessionRepository;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private final EntityManager em;

    @NotNull
    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
        this.em = entityManager;
    }

    @Override
    public boolean removeAll(String userId) {
        @NotNull final List<Session> sessions = findAll(userId);
        sessions.forEach(em::remove);
        return true;
    }

    @NotNull
    @Override
    public List<Session> findAll(String userId) {
        return em.createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setParameter("userId",userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Session> removeAll() {
        @NotNull final List<Session> sessions = findAll();
        sessions.forEach(this::remove);
        return sessions;
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return em.createQuery("SELECT e FROM Session e", Session.class).getResultList();
    }

}
