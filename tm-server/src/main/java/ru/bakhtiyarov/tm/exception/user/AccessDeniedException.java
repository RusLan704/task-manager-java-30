package ru.bakhtiyarov.tm.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    @NotNull
    public AccessDeniedException() {
        super("Error! Access Denied...");
    }

}