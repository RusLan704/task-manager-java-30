package ru.bakhtiyarov.tm.exception.invalid;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class InvalidProjectException extends AbstractException {

    @NotNull
    public InvalidProjectException() {
        super("Error! Project is invalid...");
    }

}
