package ru.bakhtiyarov.tm.exception.invalid;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class InvalidUserException extends AbstractException {

    @NotNull
    public InvalidUserException() {
        super("Error! User is invalid...");
    }

}

