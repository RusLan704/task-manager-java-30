package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.ISessionRepository;
import ru.bakhtiyarov.tm.api.repository.ITaskRepository;
import ru.bakhtiyarov.tm.api.service.IManagerFactoryService;
import ru.bakhtiyarov.tm.api.service.IPropertyService;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;
import ru.bakhtiyarov.tm.repository.SessionRepository;
import ru.bakhtiyarov.tm.repository.TaskRepository;
import ru.bakhtiyarov.tm.util.HashUtil;
import ru.bakhtiyarov.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    public final IManagerFactoryService managerFactoryService;

    @NotNull
    public final ServiceLocator serviceLocator;

    @NotNull
    public SessionService(@NotNull IManagerFactoryService managerFactoryService, @NotNull ServiceLocator serviceLocator) {
        super(managerFactoryService);
        this.managerFactoryService = managerFactoryService;
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session open(@Nullable String login, @Nullable String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();

        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();

        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ISessionRepository sessionRepository = new SessionRepository(em);

        @NotNull Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sign(session);
        session.setUser(user);
        @Nullable Session resultSession = null;
        try {
            em.getTransaction().begin();
            resultSession = sessionRepository.persist(session);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return resultSession;
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        if (password == null || password.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(password);
        if (passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session sign(@Nullable Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        if (signature == null) throw new AccessDeniedException();
        session.setSignature(signature);
        return session;
    }

    @SneakyThrows
    public void validate(@Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
    }

    @SneakyThrows
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void signOutByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String userId = user.getId();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ISessionRepository sessionRepository = new SessionRepository(em);
        try {
            em.getTransaction().begin();
            sessionRepository.removeAll(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void signOutByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ISessionRepository sessionRepository = new SessionRepository(em);
        try {
            em.getTransaction().begin();
            sessionRepository.removeAll(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void close(@Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        validate(session);
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ISessionRepository sessionRepository = new SessionRepository(em);
        try {
            em.getTransaction().begin();
            sessionRepository.remove(session);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean closeAll(@Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        validate(session);
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ISessionRepository sessionRepository = new SessionRepository(em);
        boolean response = false;
        try {
            em.getTransaction().begin();
            response = sessionRepository.removeAll(session.getUserId());
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return response;
    }

    @Override
    public List<Task> removeAll() {
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        List<Task> tasks = new ArrayList<>();
        try {
            em.getTransaction().begin();
            tasks = taskRepository.removeAll();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return tasks;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ISessionRepository sessionRepository = new SessionRepository(em);
        return sessionRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Session> findAll(@Nullable Session session) {
        validate(session);
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ISessionRepository taskRepository = new SessionRepository(em);
        return taskRepository.findAll(session.getUserId());
    }

}
