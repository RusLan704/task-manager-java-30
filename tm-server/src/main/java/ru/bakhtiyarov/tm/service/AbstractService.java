package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.IRepository;
import ru.bakhtiyarov.tm.api.service.IManagerFactoryService;
import ru.bakhtiyarov.tm.api.service.IService;
import ru.bakhtiyarov.tm.entity.AbstractEntity;
import ru.bakhtiyarov.tm.exception.invalid.InvalidTaskException;
import ru.bakhtiyarov.tm.repository.AbstractRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    public final IManagerFactoryService managerFactoryService;

    @NotNull
    public AbstractService(@NotNull IManagerFactoryService managerFactoryService) {
        this.managerFactoryService = managerFactoryService;
    }

    @Override
    public void addAll(@Nullable List<E> records) {
        if (records == null) return;
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IRepository<E> repository = new AbstractRepository<>(em);
        try {
            em.getTransaction().begin();
            repository.addAll(records);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public E persist(@Nullable final E record) {
        if (record == null) throw new InvalidTaskException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IRepository<E> repository = new AbstractRepository<>(em);
        E newRecord = null;
        try {
            em.getTransaction().begin();
            newRecord = repository.persist(record);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return newRecord;
    }

    @Override
    @Nullable
    @SneakyThrows
    public E merge(@Nullable final E record) {
        if (record == null) return null;
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IRepository<E> repository = new AbstractRepository<>(em);
        E newRecord = null;
        try {
            em.getTransaction().begin();
            newRecord = repository.merge(record);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return newRecord;
    }

}