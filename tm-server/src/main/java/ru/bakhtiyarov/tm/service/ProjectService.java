package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.IProjectRepository;
import ru.bakhtiyarov.tm.api.service.IManagerFactoryService;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.exception.empty.EmptyDescriptionException;
import ru.bakhtiyarov.tm.exception.empty.EmptyIdException;
import ru.bakhtiyarov.tm.exception.empty.EmptyNameException;
import ru.bakhtiyarov.tm.exception.empty.EmptyUserIdException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidUserException;
import ru.bakhtiyarov.tm.exception.system.IncorrectIndexException;
import ru.bakhtiyarov.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    public final IManagerFactoryService managerFactoryService;

    @NotNull
    public final ServiceLocator serviceLocator;

    @NotNull
    public ProjectService(@NotNull IManagerFactoryService managerFactoryService, @NotNull ServiceLocator serviceLocator) {
        super(managerFactoryService);
        this.managerFactoryService = managerFactoryService;
        this.serviceLocator = serviceLocator;
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new InvalidUserException();
        @Nullable Project project = new Project(name);
        project.setUser(user);
        persist(project);
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new InvalidUserException();
        @NotNull Project project = new Project(name, description);
        project.setUser(user);
        persist(project);
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IProjectRepository projectRepository = new ProjectRepository(em);
        try {
            em.getTransaction().begin();
            projectRepository.removeAll(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IProjectRepository projectRepository = new ProjectRepository(em);
        Project project = new Project();
        try {
            em.getTransaction().begin();
            project = projectRepository.removeOneByIndex(userId, index);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IProjectRepository projectRepository = new ProjectRepository(em);
        return projectRepository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IProjectRepository projectRepository = new ProjectRepository(em);
        return projectRepository.findAllByUserId();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IProjectRepository projectRepository = new ProjectRepository(em);
        Project project = new Project();
        try {
            em.getTransaction().begin();
            project = projectRepository.removeOneById(userId, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IProjectRepository projectRepository = new ProjectRepository(em);
        Project project = new Project();
        try {
            em.getTransaction().begin();
            project = projectRepository.removeOneByName(userId, name);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return project;
    }

    @Override
    public List<Project> removeAll() {
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IProjectRepository projectRepository = new ProjectRepository(em);
        List<Project> projects = new ArrayList<>();
        try {
            em.getTransaction().begin();
            projects = projectRepository.removeAll();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return merge(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return merge(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IProjectRepository projectRepository = new ProjectRepository(em);
        return projectRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IProjectRepository projectRepository = new ProjectRepository(em);
        return projectRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull IProjectRepository projectRepository = new ProjectRepository(em);
        return projectRepository.findOneByName(userId, name);
    }

}