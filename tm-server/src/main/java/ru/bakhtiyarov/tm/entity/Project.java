package ru.bakhtiyarov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_project")
public class Project extends AbstractEntity {

    @NotNull
    @Column(nullable = false, columnDefinition = "TINYTEXT")
    private String name = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    private Date startDate;

    @Nullable
    private Date completeDate;

    @Nullable
    @Column(updatable = false)
    private Date creationDate = new Date(System.currentTimeMillis());

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    public Project(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

}
