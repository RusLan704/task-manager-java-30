package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.repository.ISessionRepository;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.const_test.ConstTest;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.dto.UserDTO;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;
import ru.bakhtiyarov.tm.marker.UnitCategory;
import ru.bakhtiyarov.tm.repository.SessionRepository;

import java.util.List;

@Category(UnitCategory.class)
public class SessionDTOServiceTest {

 /*   @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final ISessionService sessionService = new SessionService(serviceLocator, sessionRepository);

    @Before
    public void addUser() {
        serviceLocator.getUserService().create("test", "test", "mail.ru");
    }

    @After
    public void removeUser() {
        serviceLocator.getUserService().removeByLogin("login");
    }

    @Test
    public void testOpen() {
        @Nullable final SessionDTO sessionDTO = sessionService.open("test", "test");
        Assert.assertNotNull(sessionDTO);
        @Nullable final SessionDTO testSessionDTO = sessionRepository.findById(sessionDTO.getId());
        Assert.assertNotNull(testSessionDTO);
        Assert.assertEquals(sessionDTO.getId(), testSessionDTO.getId());
    }

    @Test
    public void testCheckDataAccess() {
        Assert.assertTrue(sessionService.checkDataAccess("test", "test"));
        Assert.assertFalse(sessionService.checkDataAccess(ConstTest.FALSE_LOGIN, "test"));
        Assert.assertFalse(sessionService.checkDataAccess("test", ConstTest.FALSE_PASSWORD));
        Assert.assertFalse(sessionService.checkDataAccess(ConstTest.FALSE_LOGIN, ConstTest.FALSE_PASSWORD));
    }

    @Test
    public void testSign() {
        @Nullable final SessionDTO sessionDTO = new SessionDTO();
        Assert.assertNotNull(sessionService.sign(sessionDTO));
        Assert.assertNotNull(sessionDTO.getSignature());
    }

    @Test
    public void testIsValid() {
        @Nullable final SessionDTO sessionDTO = sessionService.open("test", "test");
        Assert.assertNotNull(sessionDTO);
        Assert.assertTrue(sessionService.isValid(sessionDTO));
        Assert.assertFalse(sessionService.isValid(null));
        Assert.assertFalse(sessionService.isValid(new SessionDTO()));
    }

    @Test
    public void testSignOutByUserId() {
        @Nullable final UserDTO userDTO = serviceLocator.getUserService().findByLogin("test");
        Assert.assertNotNull(userDTO);
        sessionService.open(userDTO.getLogin(), "test");
        Assert.assertEquals(1, sessionService.findAll().size());
        sessionService.signOutByUserId(userDTO.getId());
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void testSignOutByLogin() {
        @Nullable final UserDTO userDTO = serviceLocator.getUserService().findByLogin("test");
        Assert.assertNotNull(userDTO);
        sessionService.open(userDTO.getLogin(), "test");
        Assert.assertEquals(1, sessionService.findAll().size());
        sessionService.signOutByLogin(userDTO.getLogin());
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void testClose() {
        @Nullable SessionDTO sessionDTO = sessionService.open("test", "test");
        Assert.assertNotNull(sessionDTO);
        Assert.assertEquals(1, sessionService.findAll().size());
        sessionService.close(sessionDTO);
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void testCloseAll() {
        @Nullable SessionDTO sessionDTO = sessionService.open("test", "test");
        sessionService.open("test", "test");
        sessionService.open("test", "test");
        sessionService.open("test", "test");
        Assert.assertNotNull(sessionDTO);
        Assert.assertEquals(4, sessionService.findAll().size());
        sessionService.closeAll(sessionDTO);
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void testGetUser() {
        @Nullable SessionDTO sessionDTO = sessionService.open("test", "test");
        @Nullable final UserDTO userDTO = sessionService.getUser(sessionDTO);
        Assert.assertNotNull(userDTO);
        Assert.assertEquals(sessionDTO.getUserId(), userDTO.getId());
    }

    @Test
    public void testGetUserId() {
        @Nullable SessionDTO sessionDTO = sessionService.open("test", "test");
        @Nullable final String userId = sessionService.getUserId(sessionDTO);
        Assert.assertNotNull(userId);
        Assert.assertEquals(sessionDTO.getUserId(), userId);
    }

    @Test
    public void testGetListenSession() {
        @Nullable SessionDTO sessionDTO = sessionService.open("test", "test");
        sessionService.open("test", "test");
        sessionService.open("test", "test");
        sessionService.open("test", "test");
        List<SessionDTO> sessionDTOS = sessionService.getListSession(sessionDTO);
        Assert.assertNotNull(sessionDTOS);
        Assert.assertEquals(4, sessionDTOS.size());
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeOpenWithFalseCheck() {
        sessionService.open(ConstTest.FALSE_LOGIN, "test");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeOpenWithLockUser() {
        @Nullable final UserDTO userDTO = serviceLocator.getUserService().findByLogin("test");
        userDTO.setLocked(true);
        sessionService.open("test", "test");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeCheckDataAccessWithEmptyLogin() {
        sessionService.checkDataAccess("", "password");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeCheckDataAccessWithNullLogin() {
        sessionService.checkDataAccess(null, "password");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeCheckDataAccessWithEmptyPassword() {
        sessionService.checkDataAccess("test", "");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeCheckDataAccessWithNullPassword() {
        sessionService.checkDataAccess("test", null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithNullSession() {
        sessionService.validate(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithEmptySignature() {
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setSignature("");
        sessionService.validate(sessionDTO);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithNullSignature() {
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setSignature(null);
        sessionService.validate(sessionDTO);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithEmptyUserId() {
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId("");
        sessionService.validate(sessionDTO);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithNullUserId() {
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(null);
        sessionService.validate(sessionDTO);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithNullTimeStamp() {
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setTimestamp(null);
        sessionService.validate(sessionDTO);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithUnequalSignature() {
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId("123");
        sessionDTO.setTimestamp(123L);
        sessionDTO.setSignature("321");
        sessionService.validate(sessionDTO);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithRoleWithNullRole() {
        sessionService.validate(new SessionDTO(), null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithRoleWithNullUser() {
        sessionService.validate(new SessionDTO(), Role.ADMIN);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithRoleWithNullUserRole() {
        @Nullable final UserDTO userDTO = serviceLocator.getUserService().findByLogin("test");
        userDTO.setRole(null);
        sessionService.validate(new SessionDTO(), Role.ADMIN);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithRoleWithUnequalUserRole() {
        @Nullable final UserDTO userDTO = serviceLocator.getUserService().findByLogin("test");
        userDTO.setRole(Role.USER);
        sessionService.validate(new SessionDTO(), Role.ADMIN);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeSignOutByLoginWithEmptyLogin() {
        sessionService.signOutByLogin("");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeSignOutByLoginWithNullLogin() {
        sessionService.signOutByLogin(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeSignOutByLoginWithIncorrectLogin() {
        sessionService.signOutByLogin(ConstTest.FALSE_LOGIN);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeSignOutByUserIdWithEmptyUserId() {
        sessionService.signOutByUserId("");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeSignOutByUserIdWithNullUserId() {
        sessionService.signOutByUserId(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeCloseWithNullSession() {
        sessionService.close(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeCloseAllWithNullSession() {
        sessionService.closeAll(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeGetUserWithNullSession() {
        sessionService.getUser(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeGetUserWithIncorrectSession() {
        sessionService.getUser(new SessionDTO());
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeGetUserIdWithNullSession() {
        sessionService.getUserId(null);
    }
*/
}
