#!/bin/bash

PORT="8080"
if [ -n "$1" ]; then
PORT=$1
fi

FILE_PID="tm-server-$PORT"
echo  "SHUTDOWN TASK MANAGER SERVER...";
if [ ! -f ./"$FILE_PID".pid ]; then
       echo "$FILE_PID PID NOT FOUND"
       exit 1;
fi

echo "KILL PROCESS WITH PID "$(cat ./"$FILE_PID".pid);
kill -9 $(cat ./"$FILE_PID".pid)
rm ./"$FILE_PID".pid
echo "OK"




