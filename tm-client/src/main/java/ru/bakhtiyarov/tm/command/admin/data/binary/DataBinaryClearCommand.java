package ru.bakhtiyarov.tm.command.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

public final class DataBinaryClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-bin-clear";
    }


    @Override
    public String description() {
        return "Clear bin file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN CLEAR]");
        @NotNull SessionDTO session = serviceLocator.getSessionService().getSession();
        endpointLocator.getAdminDataEndpoint().clearBinary(session);
        System.out.println("[OK]");
    }

}


