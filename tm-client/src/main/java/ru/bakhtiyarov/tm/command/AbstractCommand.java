package ru.bakhtiyarov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.locator.EndpointLocator;
import ru.bakhtiyarov.tm.api.locator.ServiceLocator;
import ru.bakhtiyarov.tm.enumeration.Role;

public abstract class AbstractCommand {

    @NotNull
    protected EndpointLocator endpointLocator;

    @NotNull
    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setEndpointLocator(
            @NotNull EndpointLocator endpointLocator,
            @NotNull ServiceLocator serviceLocator
    ) {
        this.endpointLocator = endpointLocator;
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute() throws Exception;

}
