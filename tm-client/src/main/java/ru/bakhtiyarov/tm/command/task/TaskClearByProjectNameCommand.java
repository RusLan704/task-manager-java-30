package ru.bakhtiyarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.TaskEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public class TaskClearByProjectNameCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "tasks-clear-by-project-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks by project name.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        System.out.println("ENTER PROJECT NAME:");
        final String projectName = TerminalUtil.nextLine();
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @NotNull SessionDTO session = serviceLocator.getSessionService().getSession();
        taskEndpoint.removeAllByUserIdAndProjectName(session, projectName);
        System.out.println("[OK]");
    }

}