package ru.bakhtiyarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    @NotNull
    List<AbstractCommand> getCommandList();

}

